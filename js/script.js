var titulo = document.querySelector('.scroll-animation-container');
let header = document.querySelector('header');

window.addEventListener('scroll', function (e) {
    titulo.style.marginTop = -window.pageYOffset / 1.9 + 'px';
    header.style.height = 300 + window.pageYOffset / 1.5 + 'px';
});
